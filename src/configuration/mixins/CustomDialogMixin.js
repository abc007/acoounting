export default () => {
  return {
    props: {
      value: {
        type: Boolean,
        default: false
      }
    },
    methods: {
      $close() {
        this.$emit('input', false)
      },

      $open() {
        this.$emit('input', true)
      }
    }
  }
}