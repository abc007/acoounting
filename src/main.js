import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify/index';
import VueNotifications from 'vue-notification'
import VeeValidate from './plugins/vee-validate/vee-validate'
import http from './plugins/axios'
import Configuration from './configuration'
import './assets/scss/style.scss'
import VueMask from 'v-mask'
import TokenService from "@/utils/TokenService";

Vue.use(VueMask);

Vue.use(Configuration)
Vue.use(VueNotifications)
Vue.prototype.$http = http
Vue.use(VeeValidate)
Vue.config.productionTip = false;

(async function () {
  const token = TokenService.getToken()
  if (token) {
    try {
      await store.dispatch('getUser')
    } catch (e) {
      console.error('Failed to get user data')
    }
  }
  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app')
})()
