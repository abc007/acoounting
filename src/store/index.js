import Vue from 'vue'
import Vuex from 'vuex'
import loading from "./loading";
import group from "@/store/group";
import atm from "@/store/atm";
import settings from "@/store/settings";
import http from "@/plugins/axios";
import TokenService from "@/utils/TokenService";
import Actions from './actions'
import {ROLES} from "@/utils/constants";
import { getField, updateField } from 'vuex-map-fields'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    me: null,
    controlValue: {
      showLayout: false,
      floatLayout: false,
      enableDownload: true,
      previewModal: false,
      paginateElementsByHeight: 1100,
      manualPagination: false,
      filename: 'ofb',
      pdfQuality: 2,
      pdfFormat: 'a4',
      pdfOrientation: 'portrait',
      pdfContentWidth: '800px'
    }
  },
  getters: {
    getField,
    permissions: state => {
      return state.me?.permissions || []
    },
    isUserFromMainBranch(state) {
      if(state.me) {
        return  state.me.isMainBranch
      }
      return false
    },
    isAdmin: state => {
      if(state.me) {
        return state.me.roles.find(item => item === ROLES.CM_ADMINS)
      }
      return false
    },
    me: state => state.me,
    role: state => {
      if (state.me) {
        return state.me.roles[0]
      }
      return null
    }
  },
  mutations: {
    updateField,
    clearUser(state) {
      state.me = null
    },
  },
  actions: {
    async login({state, dispatch}, user) {
      try {
        const {data: {token}} = await http.post('auth', {
          ...user,
        })
        TokenService.setToken(token)
      } catch (e) {
        return Promise.reject(e)
      }
    },
    // async login({state, dispatch}, user) {
    //   try {
    //     const {data: {access_token}} = await http.post('login', {
    //       ...user,
    //     })
    //     TokenService.setToken(access_token)
    //   } catch (e) {
    //     return Promise.reject(e)
    //   }
    // },
    async getUser({state}) {
      try {
        const {data} = await http.post('users/me')
        if (data) {
          localStorage.setItem('role', 'admin')
          state.me = data
          return Promise.resolve()
        }
      } catch (e) {
        return Promise.reject(e)
      }
    },
  },
  modules: {
    loading,
    Actions,
    group,
    atm,
    settings
  }
})
