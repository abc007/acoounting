import {http} from "@/plugins/axios";

export default {
  actions: {
    async searchUser(_, params) {
      if (!params) params = {}
      try {
        const {data: {data}} = await http.get('admin/users/search', {params})
        return data
      } catch (e) {
        return Promise.reject(e)
      }
    }
  }
}