import http from "@/plugins/axios";

const actions = {
  async getAtmList(_, params = {}) {
    try {
      const {data: {content, totalPages}} = await http.post('atms/short-search', params)
      return  {content, totalPages}
    }catch (e) {
      return Promise.reject(e)
    }
  },
  async getAtmById(_, id) {
    try {
      const {data} = await http.get(`atms/get/${id}`, {
        params: {
          mfo: "00999"
        }
      })
      return data
    }
    catch (e) {
      return Promise.reject(e)
    }
  }
}

export default {
  actions
}
