import http from "@/plugins/axios";

const actions = {
  async getGroupList() {
    try {
      const {data: {content, totalPages}} = await http.post('groups/search', {})
      return {
        content, totalPages
      }
    } catch (e) {
      return Promise.reject(e)
    }
  },

  async getCasseteByAtmId(_, atmId) {
    try {
      const {data} = await http.post('atm-cassettes/get-by-atm', {
        atmId
      })
      return  data
    }catch (e) {
      return Promise.reject(e)
    }
  }
}

export default {actions}
