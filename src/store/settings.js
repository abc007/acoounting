const state = {
  dailyTransactionDate: null,
  isMobile: window.innerWidth < 768
}


const getters = {
  isSelectedTransactionDate: state => !!state.dailyTransactionDate,
  isMobile: state => state.isMobile
}


export default {
  state,
  getters
}
