export default {
    state: {
        loading: false
    },
    getters: {
      loading: state => state.loading
    },
    mutations: {
        setLoading: (state, payload) => state.loading = payload
    }
}