import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import Components from './components'
import '@fortawesome/fontawesome-free/css/all.css'
import ru from 'vuetify/lib/locale/ru'
import 'vuetify/dist/vuetify.css'

Vue.use(Components)


Vue.use(Vuetify);

export default new Vuetify({
    lang: {
        locales: {ru},
        current: 'ru'
    },
    theme: {
        themes: {
            light: {
                white: '#fff',
                success: '#2ED47A',
                // primary: '#1f74ff',
                primary: '#dda24d',
                warning: '#FFB946',
                error: '#F7685B',
                secondary: '#334D6E',
                accent: '#82B1FF',
                info: '#109CF1',
                blueviolet: "#272B48"
            },
            dark: {
                blueviolet: "#272B48",
            }
        },
        customProperties: true,
    },
    icons: {
        iconfont: 'fa',
    },
});
