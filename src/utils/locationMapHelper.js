/**
 *
 * @param lat {number}
 * @param lng{number}
 * @param type {"mobileYandex" | "mobileGoogle" | "yandexDesktop"}
 * @return {string}
 */
export function constructMapsUrl(lng, lat,  type) {
  let res = ''
  if (type === 'mobileYandex') {
    res = "yandexmaps://maps.yandex.ru";
    res += `?pt=${encodeURIComponent(`${lat},${lng}`)}&z=18&l=map`
  }

  if (type === 'yandexDesktop') {
    res = `https://yandex.ru/maps/?pt=${lat},${lng}&z=18&l=map`
  }

  if (type === 'mobileGoogle') {
    res = `https://www.google.com/maps/?q=ATM&center${lat},${lng},18z`
  }
  return res
}


