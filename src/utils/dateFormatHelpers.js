export const padStart = (value, count, addedSymbol) => {
  value = `${value}`
  if (value.length >= count) return value
  for (let i = 1; i < count; i++) {
    value = `${addedSymbol}${value}`
  }
  return value
}
export const getUTCFullYear = (date) => {
  const year = date.getUTCFullYear()
  const month = date.getMonth() + 1
  const day = date.getUTCDate()
  return `${year}-${padStart(month, 2, '0')}-${padStart(day, 2, '0')}`
}

export const getUTCFullTime = (date) => {
  const hours = date.getUTCHours()
  const minutes = date.getUTCMinutes()
  const seconds = date.getUTCSeconds()
  return `${padStart(hours, 2, '0')}:${padStart(minutes, 2, '0')}:${padStart(seconds, 2, '0')}`
}

const isDateValid = (date) => {
  if (!date) return false
  date = new Date(date)
  return !isNaN(new Date(date).getTime())
}

export const toDateFormat = (date) => {
  if (!isDateValid) return ''
  date = new Date(date)
  return `${date.getFullYear()}-${padStart(date.getMonth()+1, 2, 0)}-${padStart(date.getDay(), 2, 0)}`
}


export const dateToIsoFormat = (date) => {
  date = new Date(date)
  return `${getUTCFullYear(date)}T${getUTCFullTime(date)}.009Z`
}

export const dateFormatIso = (date) => {
  return date && !isNaN(new Date(date).getTime()) ? new Date(date).toISOString().substr(0, 10) : new Date().toISOString().substr(0, 10)
}

export const timeToString = (time) => {
  time = +time
  if (time === Infinity) return ""
  const hours = time / 60
  const rHours = Math.floor(hours)
  const minutes = Math.round((hours - rHours) * 60)
  if(!rHours && !minutes) return 0
  let str = `${minutes}м.`
  if(rHours) str =  `${rHours}ч. ${minutes}м.`
  return str
}


export const dateToString = (date) => {
  if (!date) return ''
  date = new Date(date)
  if (isNaN(date)) return ''
  const monthsInEnglish = [
    "январь", "февраль", "март", "апрель", "май", "июнь", "июль", "август", "сентябрь", "октябрь", "ноябрь", "декабрь"
  ]
  return `${date.getDate()} ${monthsInEnglish[date.getMonth()]}, ${date.getFullYear()}`
}