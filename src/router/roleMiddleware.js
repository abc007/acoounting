import store from '../store/index'

export default (roles, from, next) => {
  if(roles.includes(store.getters.role)) {
    next()
  } else {
    next('/')
  }
}