import Vue from 'vue'
import VueRouter from 'vue-router'
import {PERMISSIONS} from "@/utils/constants";
import store from "@/store";

Vue.use(VueRouter)

const routes = [
  {
    path: '/auth',
    component: () => import('../layouts/AuthorizationLayout'),
    children: [
      {
        path: '/login',
        name: 'login',
        meta: {
          public: true,
          onlyWhenLoggedOut: true
        },
        component: () => import('../views/auth/login')
      }
    ]
  },
  {
    path: '',
    component: () => import('../layouts/MainLayout'),
    children: [
      {
        path: '/',
        name: 'index',
        component: () => import('../views/cabinet/index'),
        meta: {}
      },
      {
        path: '/car',
        name: 'car-list-index',
        component: () => import('../views/cabinet/car'),
        meta: {}
      },
      {
        path: '/current-analytics',
        name: 'current-analytics',
        component: () => import('../views/cabinet/current-analytics'),
        meta: {}
      },
      {
        path: '/routes',
        name: 'routes',
        component: () => import('../views/cabinet/routes'),
        meta: {}
      },
      {
        path: '/cassette-reception',
        name: 'cassette-reception',
        component: () => import('../views/cabinet/cassette-reception'),
        meta: {}
      },
      {
        path: '/user',
        name: 'user',
        component: () => import('../views/cabinet/user'),
        meta: {}
      },
      {
        path: '/agent',
        name: 'agent',
        component: () => import('../views/cabinet/agent'),
        meta: {}
      },
      {
        path: '/machine',
        name: 'machine',
        component: () => import('../views/cabinet/machine'),
        meta: {}
      },
      {
        path: '/role',
        name: 'role',
        component: () => import('../views/cabinet/role'),
        meta: {}
      },
      {
        path: '/permission',
        name: 'permission',
        component: () => import('../views/cabinet/permission'),
        meta: {}
      },
      {
        path: '/reports',
        name: 'reports',
        component: () => import('../views/cabinet/reports'),
        meta: {}
      },
      {
        path: '/cash-collection',
        name: 'cash-collection-index',
        component: () => import('../views/cabinet/cash-collection'),
        meta: {}
      },
      {
        path: '/cash-collection/:id',
        name: 'cash-collection-items',
        props: true,
        component: () => import('../views/cabinet/cash-collection-items'),
        meta: {}
      },
      {
        path: '/collection-list',
        name: 'collection-list',
        component: () => import('../views/cabinet/collection-list'),
        meta: {}
      },
      {
        path: '/cassette',
        name: 'cassette',
        component: () => import('../views/cabinet/cassette'),
        meta: {}
      },
    ]
  },
  {
    path: '/empty',
    component: () => import('../layouts/Empty'),
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

const PERMISSION_REDIRECTS = {
  [PERMISSIONS.OFFER]: {name: 'current-analytics'},
  [PERMISSIONS.ORDER_GROUP_FOR_COLLECTOR]: {name: 'cash-collection-index'},
}

router.beforeEach((to, from, next) => {
  const isAuthenticated = !!store.getters.me
  if (!isAuthenticated && to.name !== 'login') {
    return next({name: 'login'})
  }
  let samePerms = store.getters.permissions.filter(perm => Object.keys(PERMISSION_REDIRECTS).some(permRedirect => perm === permRedirect))
  if (PERMISSION_REDIRECTS[samePerms[0]] && to.path === '/') {
    return next(PERMISSION_REDIRECTS[samePerms[0]] || '/')
  }
  return next()
});

export default router
