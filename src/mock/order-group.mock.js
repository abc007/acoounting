import {ORDER_GROUP_STATUSES} from "@/utils/constants.js";
let currentId = 1
const createOrderGroup = ({id=currentId, number = 123, depositSum = 500, depositDollar = 500, statusType = ORDER_GROUP_STATUSES.EXPECTATION.key}) => {
   currentId++
    return {
      id,
      number,
      depositSum,
      depositDollar,
      statusType
    }
}

export const getOrderGroups = (n = 12) => {
  const arr = new Array(n).fill(null).map(() => createOrderGroup({}))
  return arr
}